/*
Copyright 2022 Tony Yip.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package client

import (
	"context"
	"os"
	"time"

	"github.com/hashicorp/vault/api"
	"github.com/hashicorp/vault/api/auth/kubernetes"
)

func NewClient() (_ *Client, err error) {
	return NewClientWithContext(context.Background())
}

func NewClientWithContext(ctx context.Context) (_ *Client, err error) {
	client, err := api.NewClient(api.DefaultConfig())
	if err != nil {
		return
	}

	mountPath := os.Getenv("VAULT_KUBERNETES_MOUNT_PATH")
	if mountPath == "" {
		mountPath = "kubernetes"
	}

	auth, err := kubernetes.NewKubernetesAuth(
		os.Getenv("VAULT_KUBERNETES_ROLE"),
		kubernetes.WithMountPath(mountPath),
	)
	if err != nil {
		return nil, err
	}
	ctx, cancel := context.WithTimeout(ctx, 10*time.Second)
	defer cancel()
	renewal, err := client.Auth().Login(ctx, auth)
	if err != nil {
		return nil, err
	}

	watcher, err := client.NewLifetimeWatcher(&api.LifetimeWatcherInput{
		Secret: renewal,
	})
	go watcher.Start()

	return &Client{
		Client:          client,
		lifetimeWatcher: watcher,
	}, nil
}

type Client struct {
	*api.Client
	lifetimeWatcher *api.LifetimeWatcher
}

func (c *Client) ClearToken() {
	c.Client.ClearToken()
	c.lifetimeWatcher.Stop()
}
