/*
Copyright 2022 Tony Yip.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	errs "errors"
	"fmt"
	"strings"

	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"

	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/log"

	"github.com/hashicorp/vault/api"

	crd "gitlab.com/oss-cloud/kaas/vault-operator/api/v1alpha1"
	vaultClient "gitlab.com/oss-cloud/kaas/vault-operator/controllers/client"
)

const vaultTlsCaFinalizer = "ca.vault.kaas.osscloud.gitlab.io/finalizer"

var (
	ErrInvalidMountType = errs.New("vault: invalid mount type")
)

// VaultTlsCAReconciler reconciles a VaultTlsCA object
type VaultTlsCAReconciler struct {
	client.Client
	Scheme      *runtime.Scheme
	VaultClient *vaultClient.Client
}

//+kubebuilder:rbac:groups=kaas.osscloud.gitlab.io,resources=vaulttlscas,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=kaas.osscloud.gitlab.io,resources=vaulttlscas/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=kaas.osscloud.gitlab.io,resources=vaulttlscas/finalizers,verbs=update
//+kubebuilder:rbac:groups=kaas.osscloud.gitlab.io,resources=vaulttlscas/finalizers,verbs=update
//+kubebuilder:rbac:groups=core,resources=secrets,verbs=list;watch;get

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// Modify the Reconcile function to compare the state specified by
// the VaultTlsCA object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.10.0/pkg/reconcile
func (r *VaultTlsCAReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	logger := log.FromContext(ctx)

	ca := new(crd.VaultTlsCA)
	if err := r.Get(ctx, req.NamespacedName, ca); err != nil {
		if !errors.IsNotFound(err) {
			logger.Error(err, "unable to get resource", "VaultTlsCA", req.NamespacedName)
		}
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	if ca.ObjectMeta.DeletionTimestamp.IsZero() {
		if err := r.ensureVaultMount(ctx, ca); err != nil {
			return ctrl.Result{}, err
		}
		return ctrl.Result{}, nil
	}

	if controllerutil.ContainsFinalizer(ca, vaultTlsCaFinalizer) {
		if err := r.destroyVaultMount(ctx, ca); err != nil {
			return ctrl.Result{}, err
		}
	}

	return ctrl.Result{}, nil
}

func (r *VaultTlsCAReconciler) ensureVaultMount(ctx context.Context, ca *crd.VaultTlsCA) (err error) {
	logger := log.FromContext(ctx)

	sys := r.VaultClient.Sys()
	mounts, err := sys.ListMounts()
	if err != nil {
		return err
	}

	mountPath := vaultMountPath(ca)

	if ca.Spec.KeyBits == 0 {
		if ca.Spec.KeyType == "RSA" {
			ca.Spec.KeyBits = 2048
		}
		if ca.Spec.KeyType == "EC" {
			ca.Spec.KeyBits = 256
		}
	}

	if mount, ok := mounts[mountPath+"/"]; ok {
		if mount.Type != "pki" {
			logger.Error(ErrInvalidMountType, "invalid state for mount", "type", mount.Type)
			return ErrInvalidMountType
		}
	} else {
		if err := sys.Mount(mountPath, &api.MountInput{
			Type:        "pki",
			Description: "CA by oss cloud vault operator",
		}); err != nil {
			logger.Error(err, "fail to create mount", "path", mountPath)
			return err
		}
		logger.Info("enable vault mount", "path", mountPath)

		{
			logical := r.VaultClient.Logical()
			generateType := "internal"
			if ca.Spec.Exported {
				generateType = "exported"
			}
			commonName := ca.Spec.CommonName
			if commonName == "" {
				commonName = ca.Name
			}
			secret, err := logical.Write(fmt.Sprintf("%s/root/generate/%s", mountPath, generateType), map[string]interface{}{
				"common_name": commonName,

				"ttl": ca.Spec.Duration.Duration.String(),

				"key_type": strings.ToLower(ca.Spec.KeyType),
				"key_bits": ca.Spec.KeyBits,
			})
			if err != nil {
				logger.Error(err, "fail to generate root cert")
				return err
			}

			data := map[string]string{
				"tls.crt": secret.Data["certificate"].(string),
				"ca.crt":  secret.Data["issuing_ca"].(string),
			}

			if ca.Spec.Exported {
				data["tls.key"] = secret.Data["private_key"].(string)
			}

			secretObject := &v1.Secret{
				ObjectMeta: metav1.ObjectMeta{
					Name:        ca.Name,
					Namespace:   ca.Namespace,
					Labels:      ca.ObjectMeta.GetLabels(),
					Annotations: ca.ObjectMeta.GetAnnotations(),
				},
				Type:       v1.SecretTypeTLS,
				StringData: data,
			}
			_ = ctrl.SetControllerReference(ca, secretObject, r.Scheme)
			if err := r.Create(ctx, secretObject); err != nil {
				logger.Error(err, "failed to create secret", "name", ca.Name, "namespace", ca.Namespace)
				return err
			}
		}
	}

	if err = sys.TuneMount(mountPath, api.MountConfigInput{
		MaxLeaseTTL: ca.Spec.MaxLeaseTTL.Duration.String(),
	}); err != nil {
		logger.Error(err, "failed to tune vault mount", "path", mountPath)
		return err
	}
	logger.Info("tune vault mount", "path", mountPath, "name", ca.Name, "namespace", ca.Namespace)

	ca.Status.VaultPath = mountPath
	if err = r.Status().Update(ctx, ca); err != nil {
		logger.Error(err, "fail to update status", "name", ca.Name, "namespace", ca.Namespace)
		return err
	}
	logger.Info("update status", "name", ca.Name, "namespace", ca.Namespace)

	controllerutil.AddFinalizer(ca, vaultTlsCaFinalizer)
	if err = r.Update(ctx, ca); err != nil {
		logger.Error(err, "fail to add finalizer", "name", ca.Name, "namespace", ca.Namespace)
		return err
	}
	logger.Info("added finalizer", "name", ca.Name, "namespace", ca.Namespace)

	return nil
}

func (r *VaultTlsCAReconciler) destroyVaultMount(ctx context.Context, ca *crd.VaultTlsCA) (err error) {
	logger := log.FromContext(ctx)

	mountPath := vaultMountPath(ca)
	if err = r.VaultClient.Sys().Unmount(mountPath); err != nil {
		logger.Error(err, "fail to remove vault mount", "mountPath", mountPath, "name", ca.Name, "namespace", ca.Namespace)
		return
	}

	ca.Status.VaultPath = ""
	if err = r.Status().Update(ctx, ca); err != nil {
		logger.Error(err, "fail to update status", "name", ca.Name, "namespace", ca.Namespace)
		return err
	}

	controllerutil.RemoveFinalizer(ca, vaultTlsCaFinalizer)
	if err = r.Update(ctx, ca); err != nil {
		logger.Error(err, "fail to remove finalizer", "name", ca.Name, "namespace", ca.Namespace)
	}

	return
}

// SetupWithManager sets up the controller with the Manager.
func (r *VaultTlsCAReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&crd.VaultTlsCA{}).
		Owns(&v1.Secret{}).
		Complete(r)
}

func vaultMountPath(ca *crd.VaultTlsCA) string {
	return fmt.Sprintf("vault-operator-%s-%s", ca.Namespace, ca.Name)
}
