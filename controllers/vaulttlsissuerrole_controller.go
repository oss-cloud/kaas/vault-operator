/*
Copyright 2022 Tony Yip.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"
	"os"
	"strings"

	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"

	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/log"

	certmanager "github.com/jetstack/cert-manager/pkg/apis/certmanager/v1"
	cmmeta "github.com/jetstack/cert-manager/pkg/apis/meta/v1"

	crd "gitlab.com/oss-cloud/kaas/vault-operator/api/v1alpha1"
	vaultClient "gitlab.com/oss-cloud/kaas/vault-operator/controllers/client"
)

const vaultTlsRoleFinalizer = "role.vault.kaas.osscloud.gitlab.io/finalizer"

// VaultTlsIssuerRoleReconciler reconciles a VaultTlsIssuerRole object
type VaultTlsIssuerRoleReconciler struct {
	client.Client
	Scheme      *runtime.Scheme
	VaultClient *vaultClient.Client
}

//+kubebuilder:rbac:groups=kaas.osscloud.gitlab.io,resources=vaulttlsissuerroles,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=kaas.osscloud.gitlab.io,resources=vaulttlsissuerroles/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=kaas.osscloud.gitlab.io,resources=vaulttlsissuerroles/finalizers,verbs=update
//+kubebuilder:rbac:groups=core,resources=serviceaccounts,verbs=create;list;watch
//+kubebuilder:rbac:groups=cert-manager.io,resources=issuers,verbs=create;list;watch

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// Modify the Reconcile function to compare the state specified by
// the VaultTlsIssuerRole object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.10.0/pkg/reconcile
func (r *VaultTlsIssuerRoleReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	logger := log.FromContext(ctx)

	role := new(crd.VaultTlsIssuerRole)
	if err := r.Get(ctx, req.NamespacedName, role); err != nil {
		if !errors.IsNotFound(err) {
			logger.Error(err, "unable to get resource", "VaultTlsIssuerRole", req.NamespacedName)
		}
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	if role.DeletionTimestamp.IsZero() {
		if result, err := r.ensureResources(ctx, role); err != nil {
			return result, err
		}

		return ctrl.Result{}, nil
	}

	if controllerutil.ContainsFinalizer(role, vaultTlsRoleFinalizer) {
		if err := r.destroyResource(ctx, role); err != nil {
			return ctrl.Result{}, err
		}
	}

	return ctrl.Result{}, nil
}

func (r *VaultTlsIssuerRoleReconciler) ensureResources(ctx context.Context, role *crd.VaultTlsIssuerRole) (_ ctrl.Result, err error) {
	logger := log.FromContext(ctx)
	logger.Info("start ensuring resources")

	ca := &crd.VaultTlsCA{}

	if err = r.Get(ctx, types.NamespacedName{
		Namespace: role.Namespace,
		Name:      role.Spec.CA.Name,
	}, ca); err != nil {
		if errors.IsNotFound(err) {
			logger.Error(err, "ca is not exists", "name", role.Spec.CA.Name)
			role.Status.Status = "CaNotFound"
			if err = r.Status().Update(ctx, role); err != nil {
				logger.Error(err, "failed to update status to CaNotFound")
			}
		}
		return ctrl.Result{}, err
	}

	if ca.Status.VaultPath == "" {
		role.Status.Status = "WaitingCaReady"
		if err = r.Status().Update(ctx, role); err != nil {
			logger.Error(err, "failed to update status to CaNotFound")
		}
		return ctrl.Result{Requeue: true}, client.IgnoreNotFound(err)
	}

	controllerutil.AddFinalizer(role, vaultTlsRoleFinalizer)
	if err = r.Update(ctx, role); err != nil {
		logger.Error(err, "failed to add finalizer")
		return ctrl.Result{}, err
	}

	for _, fn := range []func(context.Context, *crd.VaultTlsCA, *crd.VaultTlsIssuerRole) error{
		r.ensureCAExists,
		r.ensureServiceAccount,
		r.ensureVaultPolicy,
		r.ensureVaultServiceAccount,
		r.ensureVaultPkiRole,
	} {
		if err = fn(ctx, ca, role); err != nil {
			return ctrl.Result{}, err
		}
	}

	return
}

func (r *VaultTlsIssuerRoleReconciler) ensureCAExists(ctx context.Context, _ *crd.VaultTlsCA, role *crd.VaultTlsIssuerRole) (err error) {
	logger := log.FromContext(ctx)
	logger.Info("ensuring CA")

	if err = r.Get(ctx, types.NamespacedName{
		Namespace: role.Namespace,
		Name:      role.Spec.CA.Name,
	}, &crd.VaultTlsCA{}); err != nil {
		if errors.IsNotFound(err) {
			logger.Error(err, "ca is not exists", "name", role.Spec.CA.Name)
			role.Status.Status = "CaNotFound"
			if err = r.Status().Update(ctx, role); err != nil {
				logger.Error(err, "failed to update status to CaNotFound")
			}
		}
		return err
	}
	return nil
}

func (r *VaultTlsIssuerRoleReconciler) ensureServiceAccount(ctx context.Context, _ *crd.VaultTlsCA, role *crd.VaultTlsIssuerRole) (err error) {
	logger := log.FromContext(ctx)

	logger.Info("ensuring service account")

	if err = r.Get(ctx, types.NamespacedName{
		Namespace: role.Namespace,
		Name:      role.Name,
	}, &v1.ServiceAccount{}); err != nil && errors.IsNotFound(err) {
		sa := &v1.ServiceAccount{
			ObjectMeta: metav1.ObjectMeta{
				Name:        role.Name,
				Namespace:   role.Namespace,
				Labels:      role.Labels,
				Annotations: role.Annotations,
			},
		}
		_ = ctrl.SetControllerReference(role, sa, r.Scheme)
		if err = r.Create(ctx, sa); err != nil {
			logger.Error(err, "fail to create service account")
			return err
		}

		logger.Info("created service account")

		role.Status.Status = "ServiceAccountCreated"
		if err = r.Status().Update(ctx, role); err != nil {
			logger.Error(err, "failed to update status to ServiceAccountCreated")
			return err
		}
		return nil
	} else if err != nil {
		logger.Error(err, "fail to get service account")
		return err
	}

	return
}

func (r *VaultTlsIssuerRoleReconciler) ensureVaultPolicy(ctx context.Context, ca *crd.VaultTlsCA, role *crd.VaultTlsIssuerRole) (err error) {
	logger := log.FromContext(ctx)
	logger.Info("ensuring vault policy")

	logical := r.VaultClient.Logical()

	policyName := fmt.Sprintf("%s-%s-%s", role.Namespace, role.Spec.CA.Name, role.Name)
	path := fmt.Sprintf("sys/policy/%s", policyName)
	secret, err := logical.Read(path)
	if err != nil {
		logger.Error(err, "fail to read policy", "policy", policyName)
		return
	} else if secret != nil {
		logger.V(5).Info("policy already exists", "path", path, "role", policyName)
		return
	}

	policy := fmt.Sprintf(`path "%s/sign-verbatim/%s" { capabilities = ["create", "update"] }`, vaultMountPath(ca), role.Name)
	if _, err = logical.Write(path, map[string]interface{}{
		"policy": policy,
	}); err != nil {
		logger.Error(err, "fail to write policy", "policy", policyName)
		return
	}

	logger.Info("created policy", "policy", policyName)

	role.Status.Status = "PolicyCreated"
	if err = r.Status().Update(ctx, role); err != nil {
		logger.Error(err, "fail to update policy to PolicyCreated")
	}

	return
}

func (r *VaultTlsIssuerRoleReconciler) ensureVaultServiceAccount(ctx context.Context, _ *crd.VaultTlsCA, role *crd.VaultTlsIssuerRole) (err error) {
	logger := log.FromContext(ctx)

	logger.Info("ensuring vault service account")

	mountPath := os.Getenv("VAULT_KUBERNETES_MOUNT_PATH")
	if mountPath == "" {
		mountPath = "kubernetes"
	}

	roleName := fmt.Sprintf("%s-%s-%s", role.Namespace, role.Spec.CA.Name, role.Name)

	logical := r.VaultClient.Logical()
	path := fmt.Sprintf("auth/%s/role/%s", mountPath, roleName)
	secret, err := logical.Read(path)
	if err != nil {
		logger.Error(err, "fail to get vault role", "path", path, "role", roleName)
		return err
	} else if secret != nil {
		logger.V(5).Info("role already exists", "path", path, "role", roleName)
		return
	}

	if _, err = logical.Write(path, map[string]interface{}{
		"bound_service_account_names":      []string{role.Name},
		"bound_service_account_namespaces": []string{role.Namespace},
		"policies":                         fmt.Sprintf("%s-%s-%s", role.Namespace, role.Spec.CA.Name, role.Name),
	}); err != nil {
		logger.Error(err, "fail to create vault role", "path", path)
		return err
	}

	if role.Status.Status == "PolicyCreated" {
		role.Status.Status = "VaultAuthRoleCreated"
		if err = r.Status().Update(ctx, role); err != nil {
			logger.Error(err, "failed to update status")
		}
	}

	return
}

func (r *VaultTlsIssuerRoleReconciler) ensureVaultPkiRole(ctx context.Context, ca *crd.VaultTlsCA, role *crd.VaultTlsIssuerRole) (err error) {
	logger := log.FromContext(ctx)

	logger.Info("ensuring vault pki role")

	mountPath := vaultMountPath(ca)
	path := fmt.Sprintf("%s/roles/%s", mountPath, role.Name)

	logical := r.VaultClient.Logical()
	if secret, err := logical.Read(path); err != nil {
		logger.Error(err, "fail to read role", "mountPath", mountPath, "path", path)
		return err
	} else if secret != nil {
		logger.V(5).Info("role already exists", "mountPath", mountPath, "path", path)
	}

	if _, err = logical.Write(path, vaultPkiRoleData(&role.Spec)); err != nil {
		logger.Error(err, "fail to write role", "mountPath", mountPath, "path", path)
		return
	}

	if role.Status.Status == "VaultAuthRoleCreated" {
		role.Status.Status = "PkiRoleCreated"
		if err = r.Status().Update(ctx, role); err != nil {
			logger.Error(err, "failed to update status", "status", "PkiRoleCreated")
		}
	}

	return
}

func (r *VaultTlsIssuerRoleReconciler) ensureCertManagerIssuer(ctx context.Context, ca *crd.VaultTlsCA, role *crd.VaultTlsIssuerRole) (err error) {
	logger := log.FromContext(ctx)

	logger.Info("ensuring cert manager issuer")

	issuer := &certmanager.Issuer{}
	exists := true
	if err = r.Get(ctx, types.NamespacedName{Name: role.Name, Namespace: role.Namespace}, issuer); err != nil && errors.IsNotFound(err) {
		exists = false
		issuer = &certmanager.Issuer{
			ObjectMeta: metav1.ObjectMeta{
				Name:        role.Name,
				Namespace:   role.Namespace,
				Labels:      role.GetLabels(),
				Annotations: role.GetAnnotations(),
			},
		}
	} else if err != nil {
		logger.Error(err, "failed to get issuer")
		return
	}

	authMountPath := os.Getenv("VAULT_KUBERNETES_MOUNT_PATH")
	if authMountPath == "" {
		authMountPath = "kubernetes"
	}

	var sa v1.ServiceAccount
	_ = r.Get(ctx, types.NamespacedName{
		Namespace: role.Namespace,
		Name:      role.Name,
	}, &sa)

	issuer.Spec = certmanager.IssuerSpec{
		IssuerConfig: certmanager.IssuerConfig{
			Vault: &certmanager.VaultIssuer{
				Path:   fmt.Sprintf("%s/sign-verbatim/%s", vaultMountPath(ca), role.Name),
				Server: os.Getenv("VAULT_ADDR"),
				Auth: certmanager.VaultAuth{
					Kubernetes: &certmanager.VaultKubernetesAuth{
						Path: fmt.Sprintf("/v1/auth/%s", authMountPath),
						Role: fmt.Sprintf("%s-%s-%s", role.Namespace, role.Spec.CA.Name, role.Name),
						SecretRef: cmmeta.SecretKeySelector{
							LocalObjectReference: cmmeta.LocalObjectReference{
								Name: sa.Secrets[0].Name,
							},
							Key: "token",
						},
					},
				},
			},
		},
	}

	if exists {
		err = r.Update(ctx, issuer)
	} else {
		_ = ctrl.SetControllerReference(role, issuer, r.Scheme)
		err = r.Create(ctx, issuer)
	}

	if err != nil {
		logger.Error(err, "fail to apply cert manager issuer")
		return err
	}

	role.Status.Status = "Ready"
	if err = r.Status().Update(ctx, role); err != nil {
		logger.Error(err, "failed to update status", "status", "Ready")
	}

	return
}

func (r *VaultTlsIssuerRoleReconciler) destroyResource(ctx context.Context, role *crd.VaultTlsIssuerRole) (err error) {
	logger := log.FromContext(ctx)
	logger.Info("start destroy resources")

	for _, fn := range []func(context.Context, *crd.VaultTlsIssuerRole) (err error){
		r.destroyVaultPolicy,
		r.destroyVaultServiceAccount,
		r.destroyVaultPkiRole,
	} {
		if err = fn(ctx, role); err != nil {
			return err
		}
	}

	logger.Info("destroyed related resources")

	return nil
}

func (r *VaultTlsIssuerRoleReconciler) destroyVaultPolicy(ctx context.Context, role *crd.VaultTlsIssuerRole) (err error) {
	logger := log.FromContext(ctx)
	logger.Info("destroy vault policy")

	logical := r.VaultClient.Logical()

	policyName := fmt.Sprintf("%s-%s-%s", role.Namespace, role.Spec.CA.Name, role.Name)
	path := fmt.Sprintf("sys/policy/%s", policyName)
	_, err = logical.Read(path)
	if err != nil {
		logger.Error(err, "fail to delete policy", "policy", policyName)
		return
	}

	logger.Info("destroyed policy", "policy", policyName)

	return
}

func (r *VaultTlsIssuerRoleReconciler) destroyVaultServiceAccount(ctx context.Context, role *crd.VaultTlsIssuerRole) (err error) {
	logger := log.FromContext(ctx)

	logger.Info("destroying vault service account")

	mountPath := os.Getenv("VAULT_KUBERNETES_MOUNT_PATH")
	if mountPath == "" {
		mountPath = "kubernetes"
	}

	roleName := fmt.Sprintf("%s-%s-%s", role.Namespace, role.Spec.CA.Name, role.Name)

	logical := r.VaultClient.Logical()
	path := fmt.Sprintf("auth/%s/role/%s", mountPath, roleName)
	if _, err = logical.Delete(path); err != nil {
		logger.Error(err, "fail to get vault role", "path", path, "role", roleName)
		return err
	}

	logger.Info("destroyed vault role", "path", path, "role", roleName)

	return
}

func (r *VaultTlsIssuerRoleReconciler) destroyVaultPkiRole(ctx context.Context, role *crd.VaultTlsIssuerRole) (err error) {
	logger := log.FromContext(ctx)

	logger.Info("destroying vault pki role")

	mountPath := fmt.Sprintf("vault-operator-%s-%s", role.Namespace, role.Spec.CA.Name)
	path := fmt.Sprintf("%s/roles/%s", mountPath, role.Name)

	logical := r.VaultClient.Logical()
	if _, err = logical.Delete(path); err != nil {
		logger.Error(err, "fail to destroy role", "mountPath", mountPath, "path", path)
		return err
	}

	logger.Info("destroyed vault pki role")

	return
}

func vaultPkiRoleData(spec *crd.VaultTlsIssuerRoleSpec) (data map[string]interface{}) {
	data = map[string]interface{}{
		"allowed_domains":    spec.AllowedDomains,
		"allow_bare_domains": spec.AllowBareDomain,
		"allow_subdomains":   spec.AllowSubdomains,
		"allow_any_name":     spec.AllowAnyName,
		"key_type":           strings.ToLower(spec.KeyType),
	}

	if spec.KeyBits == 0 {
		if spec.KeyType == "RSA" {
			data["key_bits"] = 2048
		} else {
			data["key_bits"] = 256
		}
	}

	if spec.TTL != "" {
		data["ttl"] = spec.TTL
	}

	if spec.MaxTTL != "" {
		data["max_ttl"] = spec.MaxTTL
	}

	if spec.KeyUsage != nil {
		data["key_usage"] = spec.KeyUsage
	}

	if spec.KeyUsage != nil {
		data["ext_key_usage"] = spec.ExtKeyUsage
	}

	return data
}

// SetupWithManager sets up the controller with the Manager.
func (r *VaultTlsIssuerRoleReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&crd.VaultTlsIssuerRole{}).
		Owns(&v1.ServiceAccount{}).
		Complete(r)
}
