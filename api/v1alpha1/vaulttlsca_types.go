/*
Copyright 2022.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// VaultTlsCASpec defines the desired state of VaultTlsCA
type VaultTlsCASpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	// +optional
	// +kubebuilder:default:=false
	Exported bool `json:"exported"`

	// Default metadata.name
	// +optional
	CommonName string `json:"commonName,omitempty"`

	// Duration of CA cert default 10 year
	// +optional
	// +kubebuilder:default:="87600h"
	Duration metav1.Duration `json:"duration,omitempty"`

	// Config max lease (max age) for SSL cert
	// Default 30 Days
	// +optional
	// +kubebuilder:default:="8760h"
	MaxLeaseTTL metav1.Duration `json:"maxLeaseTTL,omitempty"`

	// +optional
	// +kubebuilder:default:="RSA"
	// +kubebuilder:validation:Enum:=RSA;EC
	KeyType string `json:"keyType,omitempty"`

	// Default 2048 for RSA and 256 for EC
	// +optional
	KeyBits int `json:"keyBits,omitempty"`
}

// VaultTlsCAStatus defines the observed state of VaultTlsCA
type VaultTlsCAStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	VaultPath string `json:"vaultPath"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// VaultTlsCA is the Schema for the vaulttlscas API
type VaultTlsCA struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   VaultTlsCASpec   `json:"spec,omitempty"`
	Status VaultTlsCAStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// VaultTlsCAList contains a list of VaultTlsCA
type VaultTlsCAList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []VaultTlsCA `json:"items"`
}

func init() {
	SchemeBuilder.Register(&VaultTlsCA{}, &VaultTlsCAList{})
}
