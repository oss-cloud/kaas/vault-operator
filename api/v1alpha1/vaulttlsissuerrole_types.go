/*
Copyright 2022 Tony Yip.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// VaultTlsIssuerRoleSpec defines the desired state of VaultTlsIssuerRole
type VaultTlsIssuerRoleSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	CA v1.LocalObjectReference `json:"ca"`

	// +optional
	TTL string `json:"ttl,omitempty"`

	// +optional
	MaxTTL string `json:"maxTTL,omitempty"`

	AllowedDomains []string `json:"allowedDomains"`

	// +kubebuilder:default:=false
	// +optional
	AllowBareDomain bool `json:"allowBareDomain,omitempty"`

	// +kubebuilder:default:=false
	// +optional
	AllowSubdomains bool `json:"allowSubdomains,omitempty"`

	// +kubebuilder:default:=false
	// +optional
	AllowAnyName bool `json:"allowAnyName,omitempty"`

	// +kubebuilder:default:="RSA"
	// +kubebuilder:validation:Enum:=RSA;EC
	// +optional
	KeyType string `json:"keyType,omitempty"`

	// +optinal
	KeyBits uint `json:"keyBits,omitempty"`

	// +optional
	KeyUsage []string `json:"keyUsage,omitempty"`

	// +optional
	ExtKeyUsage []string `json:"extKeyUsage,omitempty"`
}

// VaultTlsIssuerRoleStatus defines the observed state of VaultTlsIssuerRole
type VaultTlsIssuerRoleStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	Status string `json:"status"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// VaultTlsIssuerRole is the Schema for the vaulttlsissuerroles API
type VaultTlsIssuerRole struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   VaultTlsIssuerRoleSpec   `json:"spec,omitempty"`
	Status VaultTlsIssuerRoleStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// VaultTlsIssuerRoleList contains a list of VaultTlsIssuerRole
type VaultTlsIssuerRoleList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []VaultTlsIssuerRole `json:"items"`
}

func init() {
	SchemeBuilder.Register(&VaultTlsIssuerRole{}, &VaultTlsIssuerRoleList{})
}
